$(document).ready(function () {
  $("[data-search]").click(function () {
    $("#suggestions").toggleClass("visible");
  });

  $("[data-faq]").click(function () {
    $(this).toggleClass("visible");
  });

  $("[data-overlay]").click(function () {
    $("#overlay").toggleClass("visible");
  });
});
